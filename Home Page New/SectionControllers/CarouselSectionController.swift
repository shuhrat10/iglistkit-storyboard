//
//  CarouselSectionController.swift
//  Home Page New
//
//  Created by Shukhrat Tursunov on 10/26/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import IGListKit

class CarouselSectionController: IGListSectionController, IGListSectionType {
  
  var number: Int?
  
  override init() {
    super.init()
    self.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 10)
  }
  
  func numberOfItems() -> Int {
    return 1
  }
  
  func sizeForItem(at index: Int) -> CGSize {
    let height = collectionContext?.containerSize.height ?? 0
    return CGSize(width: height, height: height)
  }
  
  func cellForItem(at index: Int) -> UICollectionViewCell {
    let cell = collectionContext!.dequeueReusableCellFromStoryboard(withIdentifier: "videoCell", for: self, at: index) as! VideoCell
    let value = number ?? 0
    cell.titleLabel.text = "\(value + 1)"
    return cell
  }
  
  func didUpdate(to object: Any) {
    number = object as? Int
  }
  
  func didSelectItem(at index: Int) {
    print("Selected item at index: \(index) with data: \(number)")
  }
  
}

//
//  StoryboardCollectionSectionCell.swift
//  Home Page New
//
//  Created by Shukhrat Tursunov on 10/26/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import IGListKit

class StoryboardCollectionSectionCell: UICollectionViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var collectionView: IGListCollectionView!
  
  override func layoutSubviews() {
    super.layoutSubviews()
    collectionView.alwaysBounceVertical = false
  }

}

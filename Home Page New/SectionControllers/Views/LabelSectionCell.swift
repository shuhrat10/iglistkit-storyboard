//
//  LabelSectionCell.swift
//  Home Page New
//
//  Created by Shukhrat Tursunov on 10/26/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class LabelSectionCell: UICollectionViewCell {

  @IBOutlet weak var textLabel: UILabel!

}

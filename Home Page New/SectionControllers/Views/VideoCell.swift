//
//  VideoCell.swift
//  Home Page New
//
//  Created by Shukhrat Tursunov on 10/26/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {

  @IBOutlet weak var coverImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  
}

//
//  ViewController.swift
//  Home Page New
//
//  Created by Shukhrat Tursunov on 10/26/16.
//  Copyright © 2016 Home. All rights reserved.
//

import UIKit
import IGListKit

class ViewController: UIViewController, IGListAdapterDataSource {
  
  @IBOutlet weak var collectionView: IGListCollectionView!
  
  lazy var adapter: IGListAdapter = {
    return IGListAdapter(updater: IGListAdapterUpdater(), viewController: self, workingRangeSize: 0)
  }()
  
  var data = [
    "Ridiculus Elit Tellus Purus Aenean",
    "Condimentum Sollicitudin Adipiscing",
    "abc",
    "swift",
    "objective c",
    "hello",
    "world",
    "new york",
    "Ligula Ipsum Tristique Parturient Euismod",
    "Purus Dapibus Vulputate",
    43,
    "Tellus Nibh Ipsum Inceptos",
    "sa"
    ] as [Any]
  
  // MARK: - Lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    adapter.collectionView = collectionView
    adapter.dataSource = self
  }

  // MARK: - Actions
  
  @IBAction func deleteButton(_ sender: AnyObject) {
    data.remove(at: 2)
    adapter.performUpdates(animated: true, completion: nil)
  }
  
  @IBAction func addNumberButton(_ sender: AnyObject) {
    data.insert(10, at: 2)
    adapter.performUpdates(animated: true, completion: nil)
  }
  
  @IBAction func addTextButton(_ sender: AnyObject) {
    data.insert("Test text", at: 2)
    adapter.performUpdates(animated: true, completion: nil)
  }
  
  //MARK: IGListAdapterDataSource
  
  func objects(for listAdapter: IGListAdapter) -> [IGListDiffable] {
    return data as! [IGListDiffable]
  }
  
  func listAdapter(_ listAdapter: IGListAdapter, sectionControllerFor object: Any) -> IGListSectionController {
    if object is Int {
      return CarouselHorizontalSectionController()
    } else {
      return LabelSectionController()
    }
  }
  
  func emptyView(for listAdapter: IGListAdapter) -> UIView? {
    return nil
  }
  
}
